﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    enum USER_ACTION_STATE
    {
        WAIT = 0,
        DEALING,
        PHASE_1_DRAWING,
        PHASE_1_DRAW_DECK,
        PHASE_1_SELECTING_DISCARD_CARD,
        PHASE_1_SELECTING_MELD_SET,
        PHASE_1_DROPPING_MELD_SET,
        PHASE_2_SELECTING_HAND_CARD,
        PHASE_2_DROPPING_MELD_SET,
        PHASE_2_SELECT_MELD_SET_FOR_LAYOFF,
        PHASE_2_DROPPING_LAYOFF_CARD,
        PHASE_2_DISCARDING,
        WAIT_FOR_OTHER_PLAYER,
        COLLECT,
        MELD,
        LAYOFF,
        DISCARD,
        WAIT_FOR_KNOCK,
        KNOCKING
    }

    private static UIManager instance;
    public static UIManager Instance()
    {
        if(instance == null)
        {
            instance = FindObjectOfType<UIManager>();
        }
        return instance;
    }

    public Transform playerHandCardsRoot;
    public Transform discardDeckRoot;
    public PlayerUI[] playerUIList;
    public TextMeshProUGUI deckCardAmountText;
    public GameObject userActionPanelRoot;
    public GameObject userActionPhase1Panel;
    public Animator userActionPhase2PanelAnimator;
    public Animator collectCardConfirmPanelAnimator;
    public Animator phase1MeldSetConfirmPanelAnimator;
    public Animator phase2LayoFfConfirmPanelAnimator;
    public Animator knockButtonAnimator;
    public Button phase1MeldButton;
    public Button phase2MeldButton;
    public Button layOffButton;
    public Button layOffConfirmButton;
    public Button discardButton;
    public TextMeshProUGUI userIDText;
    public WarningPanelManager userActionWarningManager;
    public GameObject discardingCardPanel;
    public Button discardConfirmButton;

    [Header("Camera")]
    public Camera mainCamera;
    public GameObject mainCameraRoot;
    public GameObject tableCamraRoot;
    public Transform tableCameraCardRoot;
    public MainCameraController mainCameraController;

    [Header("Deck")]
    public DeckUI deckUI;


    [Header("Card Outline Color")]
    public Color selectedColor;
    public Color discardColor;
    public Color layoffSelectColor;
    public Color normalCardColorFilter;
    public Color temporaryCardColorFilter;
    public Color[] playerColors;

    [Header("Player Turn PopUp Text")]
    public Animator popUpAnimation;
    public TextMeshProUGUI playerPopUpNameText;

    private List<DummyCard> tempPlayerHandCards = new List<DummyCard>();

    private USER_ACTION_STATE userAction;

    private PlayerUI currentPlayerUI;
    
    private List<GameObject> avialableMeldContentList = new List<GameObject>();
    DummyPlayer[] tempPlayers = null;

    private int dealCardCount;
    private int currentActivePlayerIndex;
    private int dealCardNumberPerPlayer = 7;
    private Card3D headCard;
    private List<Card3D> handSelectedCards;
    Card3D[] collectedCards;
    private Card3D collectedCard;
    private Card3D layOffCard;
    private Vector3 layOffCardOriginalPos;
    private Vector3 layOffCardOriginalAngle;
    private MeldSetUI currentSelectMeldSetUI;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        //UIStartDealCardToAllPlayers(null, 0);
        /*if (!popUpAnimation.gameObject.activeSelf)
            popUpAnimation.gameObject.SetActive(true);
        popUpAnimation.Play("ShowAndHide");*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetIntialPlayerUI(DummyPlayer[] dummyPlayers)
    {
        if (dummyPlayers.Length > playerUIList.Length)
        {
            Debug.LogError("Player UI count don't match with dummyPlayers count");
            return;
        }
        else
        {
            for (int i = 0; i < dummyPlayers.Length; i++)
            {
                if(dummyPlayers[i].playerID == Datamanager.userID)
                {
                    int count = 0;
                    int j = i;
                    while(count < dummyPlayers.Length)
                    {
                        playerUIList[count].SetPlayer(dummyPlayers[j]); // First index of PlayerUI always be the User
                        count++;
                        j++;
                        if(j >= dummyPlayers.Length)
                        {
                            j = 0;
                        }
                    }
                    break;
                }
                
            }
        }
        
    }

    public void SetActivePlayer(string playerID)
    {
        /*if (currentPlayerUI != null)
        {
            currentPlayerUI.SetDeactivePlayer();
        }
        if (playerIndex >= playerUIList.Length)
        {
            Debug.LogError("Player UI count don't match with dummyPlayers count");
            return;
        }
        currentPlayerUI = playerUIList[playerIndex];
        currentPlayerUI.SetActivePlayer();*/
        userAction = USER_ACTION_STATE.WAIT_FOR_OTHER_PLAYER;
        string playerName = "";
        foreach(PlayerUI playerUI in playerUIList)
        {
            if(playerID == playerUI.playerID)
            {
                playerName = playerUI.playerNameText.text;
                Debug.Log("Current Active Player : [" + playerID + "], " + playerUI.playerNameText.text);
            }
            if (playerID == Datamanager.userID)
            {
                SetStartUIPhase1();
            }
        }

        if (popUpAnimation != null)
        {
            if(playerID == Datamanager.userID)
            {
                playerPopUpNameText.text = "YOUR";
            }
            else
            {
                playerPopUpNameText.text = playerName;
            }
            if (!popUpAnimation.gameObject.activeSelf)
                popUpAnimation.gameObject.SetActive(true);
            popUpAnimation.Play("ShowAndHide",0,0);
        }
    }

    public void SetUpNewGameUI()
    {
        knockButtonAnimator.gameObject.SetActive(false);
    }

    public void UpdatePlayerUIData(DummyPlayer player)
    {
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerUIList[i].playerID == player.playerID)
            {
                playerUIList[i].UpdatePlayerUI(player);
                break;
            }
        }
    }

    public void UpdateDeckCardAmoutText(int deckAmount)
    {
        deckCardAmountText.text = deckAmount.ToString();
    }

    public void UIStartDealCardToAllPlayers(DummyCard[] cardsInHand,int startPlayer)
    {
        /*SetUserHandCards(cardsInHand);
        for(int i = 0;i < playerUIList.Length; i++)
        {
            playerUIList[i].UpdateHandCardsAmount(7);
        }
        UIEndDealCardToAllPlayers();*/
        tempPlayerHandCards = new List<DummyCard>(cardsInHand);
        userAction = USER_ACTION_STATE.DEALING;
        deckUI.StartShuffleAnimation();
    }

    public void UIDeckShuffleEnd()
    {
        currentActivePlayerIndex = 0;
        dealCardCount = 0;
        DealCardToPlayer(currentActivePlayerIndex);
    }

    void DealCardToPlayer(int playerIndex)
    {
        Card3D cardObject = deckUI.PopTopCard();
        if(dealCardCount % playerUIList.Length == 0)
        {
            int index = (int)(dealCardCount / playerUIList.Length);
            cardObject.SetCard(tempPlayerHandCards[index]);
        }
        playerUIList[playerIndex].DealCardToPlayerAnimation(cardObject);
        dealCardCount++;
    }

    public void DealCardToPlayerAnimationEnd()
    {
        if(userAction == USER_ACTION_STATE.PHASE_1_DRAW_DECK)
        {
            UIEndDrawState();
        }
        else if (userAction == USER_ACTION_STATE.DEALING)
        {
            if (dealCardCount < (dealCardNumberPerPlayer * playerUIList.Length))
            {
                currentActivePlayerIndex++;
                if(currentActivePlayerIndex >= playerUIList.Length)
                {
                    currentActivePlayerIndex = 0;
                }
                DealCardToPlayer(currentActivePlayerIndex);
            }
            else
            {
                UIEndDealCardToAllPlayers();
            }
        }
    }

    public void UserHandCardUpdatePositionEnd()
    {
        if (userAction == USER_ACTION_STATE.PHASE_1_SELECTING_MELD_SET)
        {
            Debug.Log("DealCardToPlayerAnimationEnd PHASE_1_SELECTING_MELD_SET");
            playerUIList[0].SetHandCardsInteractable();
            for (int i = 0; i < collectedCards.Length; i++)
            {
                if (collectedCards[i].IsSelected())
                {
                    collectedCards[i].SetPermanentSelect();
                    break;
                }
            }
            phase1MeldButton.interactable = false;
            phase1MeldSetConfirmPanelAnimator.SetBool("Show", true);
        }
    }

    void UIEndDealCardToAllPlayers()
    {
        DummyGameManager.Instance().UIDealCardToPlayersEnd();
    }

    public void UIStartDiscardHeadCard(DummyCard _headCard, int deckAmount)
    {
        //deckCardAmountText.text = deckAmount.ToString();
        //DummyCard[] dummyCards = { headCard };
        //UpdateDiscardDeck(dummyCards);
        headCard = deckUI.PopTopCard();
        headCard.SetCard(_headCard);
        Vector3 cardMoveToPosition = DiscardDeckUIManager.Instance().discardRoot.transform.position;
        cardMoveToPosition.y += 0.3f;
        headCard.cardTweenAnimation.MoveTo(cardMoveToPosition, 0.5f, "easeInQuad", false, gameObject, "RotateHeadCard");
    }

    void RotateHeadCard()
    {
        Vector3 rotateTo = new Vector3(0, 0, 0);
        //Debug.Log("RotateHeadCard from " + headCard.transform.localEulerAngles.ToString() + " to " + rotateTo.ToString());
        headCard.cardTweenAnimation.RotateTo(rotateTo, 0.5f, "easeInOutQuad", true, gameObject, "MoveHeadCardToTable");
    }
    void MoveHeadCardToTable()
    {
        Card3D[] cards = { headCard };
        DiscardDeckUIManager.Instance().AddDiscardCard(cards);
        DiscardDeckUIManager.Instance().UpdateDiscardCardsPosition(true);
        Invoke("UIEndDiscardHeadCard", 0.5f);
    }

    void UIEndDiscardHeadCard()
    {
        DummyGameManager.Instance().UIDiscardingHeadCardEnd();
    }

    public void UIPlayStartingState()
    {
        UIEndStartingState();
    }

    void UIEndStartingState()
    {
        DummyGameManager.Instance().UIStartingStateEnd();
    }

    void UIEndDiscardHandCard()
    {
        DummyGameManager.Instance().UIUserDiscardHandCardEnd();
    }

    public void UIStartDrawState(DummyCard drawCard,string playerID)
    {
        if(playerID == Datamanager.userID)
        {
            userAction = USER_ACTION_STATE.PHASE_1_DRAW_DECK;
        }
        
        //Debug.Log("DrawCard " + drawCard.suit + " , " + drawCard.rank + " / Card in hands = " + handCards.Length);
        //List<DummyCard> newCardList = new List<DummyCard>(handCards);
        //newCardList.Add(drawCard);
        //SetUserHandCards(handCards);
        //UpdateDeckCardAmoutText(deckAmount);
        Card3D card3D = deckUI.PopTopCard();
        if(drawCard != null)
            card3D.SetCard(drawCard);
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerUIList[i].playerID == playerID)
            {
                playerUIList[i].DealCardToPlayerAnimation(card3D);
                break;
            }
        }
    }

    void UIEndDrawState()
    {
        DummyGameManager.Instance().UIDrawStateEnd();
    }

    public void OnKnockClick()
    {
        DummyGameManager.Instance().OnUserCallKnock();
        knockButtonAnimator.SetTrigger("hide");
    }

    public void OnLayOffClick()
    {
        //DummyGameManager.Instance().OnUserCallLayOff();
        if(userAction == USER_ACTION_STATE.PHASE_2_SELECTING_HAND_CARD)
        {
            userAction = USER_ACTION_STATE.PHASE_2_SELECT_MELD_SET_FOR_LAYOFF;
            phase2LayoFfConfirmPanelAnimator.SetBool("Show", true);
            userActionPhase2PanelAnimator.SetBool("Show", false);
            layOffConfirmButton.interactable = false;
            SwitchCameraToTable();
            playerUIList[0].SetHandCardDisableInteractable();
            layOffCard = playerUIList[0].GetHandSelectedCards()[0];
            layOffCardOriginalPos = layOffCard.transform.position;
            layOffCardOriginalAngle = layOffCard.transform.eulerAngles;
            layOffCard.transform.position = tableCameraCardRoot.position;
            layOffCard.transform.eulerAngles = tableCameraCardRoot.transform.eulerAngles;

            foreach (PlayerUI playerUI in playerUIList)
            {
                playerUI.SetMeldSetInteractable();
            }
        }
        
    }

    void OnDrawConfirm()
    {
        Debug.Log("OnDrawConfirm");
        DummyGameManager.Instance().StartDrawCard();
        if (userActionWarningManager != null)
        {
            userActionWarningManager.ClosePanel();
        }
    }

    public void OnDiscardConfirmClick()
    {
        //Debug.Log("OnDiscardConfirm");
        Card3D[] seletedCards = playerUIList[0].GetHandSelectedCards();
        if(seletedCards.Length == 1)
        {
            userAction = USER_ACTION_STATE.PHASE_2_DISCARDING;
            DummyGameManager.Instance().UserDisardCard(seletedCards[0].card);
            playerUIList[0].SetHandCardDisableInteractable();
            userActionPhase2PanelAnimator.SetBool("Show", false);
        }
        
    }
    public void UpdatePlayerMeldSet(DummyPlayer player)
    {
        for (int i = 0; i < playerUIList.Length; i++)
        {
            if (playerUIList[i].playerID == player.playerID)
            {
                //playerUIList[i].UpdatePlayerMeldSet(player.GetMeldSet());
                break;
            }
        }
    }

    public void UpdatePlayerMeldSetLayOffSelector(DummyPlayer player)
    {
        for (int i = 0; i < playerUIList.Length; i++)
        {
            if (playerUIList[i].playerID == player.playerID)
            {
                playerUIList[i].UpdatePlayerLayOffMeldSet(player.GetMeldSet(), player.playerID);
                break;
            }
        }
    }

    public void OnSelectLayOffMeldSet(MeldSetUI meldSetUI)
    {
        if(currentSelectMeldSetUI != null)
        {
            currentSelectMeldSetUI.DeactiveCardsOutline();
        }

        currentSelectMeldSetUI = meldSetUI;
        currentSelectMeldSetUI.ActiveCardsOutline();
        bool isLayOffAbleSet = DummyGameManager.Instance().IsLayOffAbleSet(layOffCard.card,meldSetUI.OwnerID(),meldSetUI.MeldIndex());
        layOffConfirmButton.interactable = isLayOffAbleSet;
    }

    public void OnCloseLayOffConfirmPanel()
    {
        userAction = USER_ACTION_STATE.PHASE_2_SELECTING_HAND_CARD;
        layOffCard.transform.position = layOffCardOriginalPos;
        layOffCard.transform.eulerAngles = layOffCardOriginalAngle;
        phase2LayoFfConfirmPanelAnimator.SetBool("Show", false);
        userActionPhase2PanelAnimator.SetBool("Show", true);
        SwitchCameraBackToMain();
        playerUIList[0].SetHandCardsInteractable();
        layOffCard.SetUnSelected();
        layOffButton.interactable = false;
        discardButton.interactable = false;
        foreach (PlayerUI playerUI in playerUIList)
        {
            playerUI.SetMeldSetDisableInteractable();
        }
    }

    public void OnLayOffConfirmClick()
    {
        if(userAction == USER_ACTION_STATE.PHASE_2_SELECT_MELD_SET_FOR_LAYOFF)
        {
            userAction = USER_ACTION_STATE.PHASE_2_DROPPING_LAYOFF_CARD;
            DummyGameManager.Instance().OnUserCallConfirmLayOff(layOffCard.card ,currentSelectMeldSetUI.OwnerID(), currentSelectMeldSetUI.MeldIndex());
        }
    }

    public void OnKnockButtonClick()
    {
        if(userAction == USER_ACTION_STATE.WAIT_FOR_KNOCK)
        {
            userAction = USER_ACTION_STATE.KNOCKING;
            DummyGameManager.Instance().OnUserCallKnock();
        }
        
    }

    public Color GetPlayerColor(string playerID)
    {
        Color c = normalCardColorFilter;
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerID == playerUIList[i].playerID)
            {
                if(i < playerColors.Length)
                {
                    c = playerColors[i];
                }
                break;
            }
        }
        return c;
    }

    public void ShowPlayerSpecialScoreUI(string playerID,int newScore, DUMMY_SCORE_TYPE score_type)
    {
        Debug.Log("UIManager : ShowPlayerSpecialScoreUI -> playerID:" + playerID + " ,score_type: " + score_type.ToString() + ",newScore " + newScore);
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerUIList[i].playerID == playerID)
            {
                playerUIList[i].playerScoreText.text = newScore.ToString();
                break;
            }
        }
    }

    public void SetUpUserUI(DummyPlayer playerData,DummyCard[] playerHandCards)
    {
        playerUIList[0].SetPlayer(playerData);
        playerUIList[0].SetHandCard(playerHandCards);
        playerUIList[0].UpdatePlayerMeldSetUIImmediately(playerData.GetMeldSet().ToArray());
    }

    public void SetNewPlayerUIData(string playerID, DummyPlayer playerData, DummyCard[] playerHandCards)
    {
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerUIList[i].playerID == playerID)
            {
                playerUIList[i].SetPlayer(playerData);
                playerUIList[i].SetHandCard(playerHandCards);
                Debug.Log(playerData.playerName + "[" + playerData.playerID + "] : meld set count = " + playerData.GetMeldSet().Count);
                playerUIList[i].UpdatePlayerMeldSetUIImmediately(playerData.GetMeldSet().ToArray());
                break;
            }
        }
    }

    public void SetStartUIPhase1()
    {
        userAction = USER_ACTION_STATE.PHASE_1_DRAWING;
        ActiveDeck();
        DiscardDeckUIManager.Instance().SetCardInteractable();
    }

    public void SetStartUIPhase2()
    {
        userAction = USER_ACTION_STATE.PHASE_2_SELECTING_HAND_CARD;
        userActionPhase2PanelAnimator.SetBool("Show", true);
        phase2MeldButton.interactable = false;
        layOffButton.interactable = false;
        discardButton.interactable = false;
        playerUIList[0].SetHandCardsInteractable();
        DiscardDeckUIManager.Instance().SetDisableCardInteractable();
    }

    public void EndPhase1Meld()
    {
        phase1MeldSetConfirmPanelAnimator.SetBool("Show", false);
    }

    public void ActiveDeck()
    {
        deckUI.SetDeckActive();
    }

    public void DeActiveDeck()
    {
        deckUI.SetDeckDeactive();
    }

    public void SetUserHandCardInteractable()
    {
        playerUIList[0].SetHandCardsInteractable();
    }

    void OnHandCardClick(Card3D card)
    {
        //Debug.Log("OnHandCardClick " + card.card.rank + "," + card.card.suit);

        Card3D[] selectedCard = playerUIList[0].GetHandSelectedCards();
        //Debug.Log("selectedCard count = " + selectedCard.Length);
        if(userAction == USER_ACTION_STATE.PHASE_1_SELECTING_MELD_SET)
        {
            if (selectedCard.Length >= 3)
            {
                List<DummyCard> cards = new List<DummyCard>();
                for(int i = 0;i < selectedCard.Length; i++)
                {
                    cards.Add(selectedCard[i].card);
                }
                phase1MeldButton.interactable = DummyCoreSimulator.IsMeldSet(cards.ToArray()) && (selectedCard.Length < playerUIList[0].GetHandCards().Length);
            }
            else
            {
                phase1MeldButton.interactable = false;
            }
        }
        else if (userAction == USER_ACTION_STATE.PHASE_2_SELECTING_HAND_CARD)
        {
            if (selectedCard.Length == 1)
            {
                discardButton.interactable = true;
                layOffButton.interactable = DummyGameManager.Instance().IsLayOffAble();
                phase2MeldButton.interactable = false;
            }
            else if (selectedCard.Length > 1)
            {
                discardButton.interactable = false;
                layOffButton.interactable = false;
                if (selectedCard.Length >= 3)
                {
                    List<DummyCard> cards = new List<DummyCard>();
                    for (int i = 0; i < selectedCard.Length; i++)
                    {
                        cards.Add(selectedCard[i].card);
                    }
                    phase2MeldButton.interactable = DummyCoreSimulator.IsMeldSet(cards.ToArray()) && (selectedCard.Length < playerUIList[0].GetHandCards().Length);
                }
                else
                {
                    phase2MeldButton.interactable = false;
                }
            }
            else
            {
                discardButton.interactable = false;
                layOffButton.interactable = false;
                phase2MeldButton.interactable = false;
            }
        }
        
    }

    public void AddDiscardDeck(string playerID,DummyCard discardCard)
    {
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerUIList[i].playerID == playerID)
            {
                Card3D card3D = null;
                if(playerID == Datamanager.userID)
                {
                    card3D = playerUIList[i].GetAndRemoveCardFromHand(discardCard);
                }
                else
                {
                    card3D = playerUIList[i].GetBlankHandCard();
                    card3D.SetCard(discardCard);
                }
                if (card3D)
                {
                    card3D.ResetCard();
                    Card3D[] card3Ds = { card3D };
                    DiscardDeckUIManager.Instance().MoveDiscardCardToDropTopPosition(card3D);
                    playerUIList[i].UpdateHandCardPositionWithDelay(0.5f);
                    if (playerID == Datamanager.userID)
                    {
                        Invoke("UIEndDiscardHandCard", 2f);
                    }
                }
                else
                {
                    Debug.LogError("Can't find discarded card in player " + playerID + " hand");
                }
            }
        }
    }

    public void OnDiscardCardClick(Card3D clickedCard)
    {
        if(userAction == USER_ACTION_STATE.PHASE_1_DRAWING)
        {
            if(DiscardDeckUIManager.Instance().GetSelectedCard() != null)
            {
                deckUI.SetDeckDeactive();
                collectCardConfirmPanelAnimator.SetBool("Show", true);
            }
            else
            {
                deckUI.SetDeckActive();
                collectCardConfirmPanelAnimator.SetBool("Show", false);
            }
        }
    }

    public void OnCollectCardClick()
    {
        Debug.Log("OnCollectCardClick");
        if (userAction == USER_ACTION_STATE.PHASE_1_DRAWING)
        {
            if (DiscardDeckUIManager.Instance().GetSelectedCard() != null)
            {
                userAction = USER_ACTION_STATE.PHASE_1_SELECTING_MELD_SET;
                collectCardConfirmPanelAnimator.SetBool("Show", false);
                DiscardDeckUIManager.Instance().SetDisableCardInteractable();
                collectedCards = DiscardDeckUIManager.Instance().GetCollectedCards();
                Debug.Log("collectedCards " + collectedCards.Length);
                for(int i = 0;i < collectedCards.Length; i++)
                {
                    if (collectedCards[i].IsSelected())
                    {
                        collectedCard = collectedCards[i];
                    }
                    collectedCards[i].SetTemporary(true);
                }
                playerUIList[0].DealCardToPlayerAnimation(collectedCards);
            }
        }
    }

    public void OnPhase1CancleCollectDiscardCardClick()
    {
        if (userAction == USER_ACTION_STATE.PHASE_1_SELECTING_MELD_SET)
        {
            userAction = USER_ACTION_STATE.PHASE_1_DRAWING;
            phase1MeldSetConfirmPanelAnimator.SetBool("Show", false);
            playerUIList[0].RemoveHandCard(collectedCards);
            playerUIList[0].UpdateHandCardPosition(true);
            playerUIList[0].ResetHandCards();
            playerUIList[0].SetHandCardDisableInteractable();
            for (int i = 0;i < collectedCards.Length; i++)
            {
                collectedCards[i].ResetCard();
            }
            DiscardDeckUIManager.Instance().AddDiscardCard(collectedCards);
            DiscardDeckUIManager.Instance().UpdateDiscardCardsPosition(false);
            DiscardDeckUIManager.Instance().SetCardInteractable();
            deckUI.SetDeckActive();
        }
    }

    public void OnPhase1MeldButtonClick()
    {
        if (userAction == USER_ACTION_STATE.PHASE_1_SELECTING_MELD_SET)
        {
            userAction = USER_ACTION_STATE.PHASE_1_DROPPING_MELD_SET;
            Card3D[] selectedCard = playerUIList[0].GetHandSelectedCards();
            if (selectedCard.Length >= 3)
            {
                List<DummyCard> cards = new List<DummyCard>();
                for (int i = 0; i < selectedCard.Length; i++)
                {
                    cards.Add(selectedCard[i].card);
                }
                if (DummyCoreSimulator.IsMeldSet(cards.ToArray()))
                {
                    phase1MeldButton.interactable = false;
                    DummyGameManager.Instance().UserRequestMeldCardsFromDiscardCard(cards.ToArray(), collectedCard.card);
                }
            }
        }
        
    }

    public void OnPhase2MeldButtonClick()
    {
        if(userAction == USER_ACTION_STATE.PHASE_2_SELECTING_HAND_CARD)
        {
            userAction = USER_ACTION_STATE.PHASE_2_DROPPING_MELD_SET;
            Card3D[] selectedCard = playerUIList[0].GetHandSelectedCards();
            if (selectedCard.Length >= 3)
            {
                List<DummyCard> cards = new List<DummyCard>();
                for (int i = 0; i < selectedCard.Length; i++)
                {
                    cards.Add(selectedCard[i].card);
                }
                if (DummyCoreSimulator.IsMeldSet(cards.ToArray()))
                {
                    phase2MeldButton.interactable = false;
                    DummyGameManager.Instance().UserRequestMeldCardsFromHand(cards.ToArray());
                }
            }
        }
        
    }

    public void UpdatePlayerScore(string playerID,int score)
    {
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerUIList[i].playerID == playerID)
            {
                playerUIList[i].UpdateScore(score);
            }
        }
    }

    public void PlayerDrawAndMeldCards(string playerID, DummyCard[] drawCards, DummyCard[] meldCards)
    {
        if(playerID != Datamanager.userID)
        {
            Card3D[] drawCard3Ds = DiscardDeckUIManager.Instance().GetCollectedCards(drawCards);
            StartCoroutine(DrawDiscardCardsAndMeld(playerID,drawCard3Ds, meldCards));
        }
        else
        {
            for (int pI = 0; pI < playerUIList.Length; pI++)
            {
                if (playerUIList[pI].playerID == playerID)
                {
                    playerUIList[pI].SetMeldCardsFromHand(meldCards);
                    break;
                }
            }
        }
    }

    IEnumerator DrawDiscardCardsAndMeld(string playerID,Card3D[] drawCards, DummyCard[] meldCards)
    {
        for(int i = 0;i < playerUIList.Length; i++)
        {
            if(playerUIList[i].playerID == playerID)
            {
                playerUIList[i].DealCardToPlayerAnimation(drawCards);
                break;
            }
        }
        yield return new WaitForSeconds(0.6f);
        for (int pI = 0; pI < playerUIList.Length; pI++)
        {
            if (playerUIList[pI].playerID == playerID)
            {
                playerUIList[pI].SetMeldCardsFromHand(meldCards);
                break;
            }
        }
    }

    public void PlayerMeldCards(string playerID,DummyCard[] meldCards)
    {
        for (int pI = 0; pI < playerUIList.Length; pI++)
        {
            if (playerUIList[pI].playerID == playerID)
            {
                playerUIList[pI].SetMeldCardsFromHand(meldCards);
                break;
            }
        }

    }

    public void StartLayOffCard(string meldSetOwnerID, int meldSetIndex, string layOffPlayerID, DummyCard _layOffCard)
    {
        Debug.Log("UIManager EndLayOffSelector");
        Card3D card3D = null;
        for (int i = 0; i < playerUIList.Length; i++)
        {
            if (layOffPlayerID == playerUIList[i].playerID)
            {
                if (layOffPlayerID == Datamanager.userID)
                {// current user || offline mode
                    card3D = layOffCard;
                    Card3D[] card3Ds = { card3D };
                    playerUIList[i].RemoveHandCard(card3Ds);
                }
                else
                {// other user // online mode
                    card3D = playerUIList[i].GetBlankHandCard();
                }
                playerUIList[i].UpdateHandCardPosition(true);
            }
        }
        if (card3D)
        {
            card3D.SetCard(_layOffCard);
            for (int i = 0; i < playerUIList.Length; i++)
            {
                if (meldSetOwnerID == playerUIList[i].playerID)
                {
                    playerUIList[i].AddLayOffCardToMeldSet(meldSetIndex, card3D);
                    break;
                }
            }
        }
    }
    public void EndLayOffSelector()
    {
        userAction = USER_ACTION_STATE.PHASE_2_SELECTING_HAND_CARD;
        phase2LayoFfConfirmPanelAnimator.SetBool("Show", false);
        userActionPhase2PanelAnimator.SetBool("Show", true);
        SwitchCameraBackToMain();
        playerUIList[0].SetHandCardsInteractable();
        layOffButton.interactable = false;
        discardButton.interactable = false;
        foreach (PlayerUI playerUI in playerUIList)
        {
            playerUI.SetMeldSetDisableInteractable();
        }
    }

    public void SwitchCameraBackToMain()
    {
        SwitchCameraTransform(mainCameraRoot.transform);
        mainCameraController.ActiveController();
    }

    void SwitchCameraToTable()
    {
        SwitchCameraTransform(tableCamraRoot.transform);
        mainCameraController.DeactiveController();
    }
    void SwitchCameraTransform(Transform toTransform)
    {
        mainCamera.transform.parent = toTransform;
        mainCamera.transform.localPosition = Vector3.zero;
        mainCamera.transform.localEulerAngles = Vector3.zero;
    }

    public void SetUserHandCards(DummyCard[] newCards)
    {
        playerUIList[0].SetHandCard(newCards);
    }

    public void GoToUIKnockState()
    {
        playerUIList[0].SetHandCardDisableInteractable();
        userAction = USER_ACTION_STATE.WAIT_FOR_KNOCK;
        knockButtonAnimator.gameObject.SetActive(true);
        userActionPhase2PanelAnimator.SetBool("Show", false);
        phase2LayoFfConfirmPanelAnimator.SetBool("Show", false);
        SwitchCameraBackToMain();
    }

}
