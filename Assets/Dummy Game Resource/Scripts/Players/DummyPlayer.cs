using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyPlayer
{
    public string playerID = "";
    public string playerName = "";
    public int score = 0;
    public int handCardsAmount = 0;
    private List<DummyCard> handCards = new List<DummyCard>();
    private List<MeldSet> meldSets = new List<MeldSet>();
    private List<DummyCard> layOFfCards = new List<DummyCard>();
    private List<DUMMY_SCORE_TYPE> dummyScoreTypes = new List<DUMMY_SCORE_TYPE>();
    public bool isFirstMeld = false;
    public bool isKnocker = false;
    

    public void AddCardsToHand(DummyCard[] cards)
    {
        for(int i = 0;i < cards.Length; i++)
        {
            handCards.Add(cards[i]);
        }
    }

    public void AddCardToHand(DummyCard card)
    {
        handCards.Add(card);
    }

    public void AddLayOffCard(DummyCard card)
    {
        layOFfCards.Add(card);
    }

    public void RemoveCardFromHand(DummyCard[] cards)
    {
        for(int i = 0;i < cards.Length; i++)
        {
            handCards.Remove(cards[i]);
        }
    }

    public void RemoveCardFromHand(DummyCard removeCard)
    {
        foreach(DummyCard card in handCards)
        {
            if (card.Equal(removeCard))
            {
                handCards.Remove(card);
                break;
            }
        }
        
    }

    public void AddMeldSet(MeldSet meldSet)
    {
        meldSet.playerID = playerID;
        if (meldSets.Count == 0)
            isFirstMeld = true;
        meldSets.Add(meldSet);
        Debug.Log(playerID + " AddMeldSet , meldSet.GetMeldCardSet().Length = " + meldSet.GetMeldCardSet().Length + " , meldSets.Cout = " + meldSets.Count);
    }

    public void SetMeldSet(List<MeldSet> _meldSets)
    {
        meldSets = _meldSets;
    }

    public void UpdateMeldSet(int meldSetIndex,MeldSet newMeldSet)
    {
        if(meldSetIndex >= 0 && meldSetIndex < meldSets.Count)
        {
            meldSets[meldSetIndex] = newMeldSet;
        }
    }

    public void AddDummyScoreType(DUMMY_SCORE_TYPE dummyScoreType)
    {
        if (!dummyScoreTypes.Contains(dummyScoreType))
        {
            dummyScoreTypes.Add(dummyScoreType);
        }
    }

    public void UpdateDummyScoreType(DUMMY_SCORE_TYPE[] _dummyScoreTypes)
    {
        dummyScoreTypes = new List<DUMMY_SCORE_TYPE>(_dummyScoreTypes);
    }

    public List<DummyCard> GetHandCards()
    {
        return handCards;
    }

    public List<MeldSet> GetMeldSet()
    {
        return meldSets;
    }

    public List<DUMMY_SCORE_TYPE> GetDummyScoreType()
    {
        return dummyScoreTypes;
    }

    public void Knock()
    {
        if(handCards.Count == 1)
        {
            if (handCards[0].IsSpeto())
            {
                AddDummyScoreType(DUMMY_SCORE_TYPE.KNOCK_SPETO);
            }
            if (isFirstMeld)
            {
                AddDummyScoreType(DUMMY_SCORE_TYPE.KNOCK_DARK);
            }
            bool isColorKnock = true;

            if (isColorKnock)
            {
                AddDummyScoreType(DUMMY_SCORE_TYPE.KNOCK_COLOR);
            }
        }
    }

    public void ResetRound()
    {
        isFirstMeld = false;
        dummyScoreTypes.Clear();
        handCards.Clear();
        layOFfCards.Clear();
        isKnocker = false;
    }
}
