﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class HttpRequest
{
    private readonly string _baseUrl;
    private string UserToken;
    private UnityWebRequest RequestInstance;

    private string defaultUrl = "https://apitest.landofcannabis.io/api";
    
    public HttpRequest()
    {
        //this._baseUrl = PlayerPrefsHelper.GameServerEndpoint;
        this._baseUrl = defaultUrl;
    }

    
    public UnityWebRequest Post(string MethodName, string Payload)
    {
        Debug.Log("UnityWebRequest Post : " + $"{this._baseUrl}/{MethodName}");
        UnityWebRequest request = UnityWebRequest.Post($"{this._baseUrl}/{MethodName}", "");
        if (!String.IsNullOrEmpty(Payload))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(Payload);
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        }
        request.SetRequestHeader("content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        //if (Storage.Auth() != null)
        //{
        //    request.SetRequestHeader("accessToken", $"{Storage.Auth().AccessToken}" );
        //}
        return request;
    }

    public UnityWebRequest PostWithToken(string MethodName, string Payload, string token)
    {
        Debug.Log("UnityWebRequest Post : " + $"{this._baseUrl}/{MethodName}");
        UnityWebRequest request = UnityWebRequest.Post($"{this._baseUrl}/{MethodName}", "");
        if (!String.IsNullOrEmpty(Payload))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(Payload);
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        }
        request.SetRequestHeader("content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        request.SetRequestHeader("authentication", "Bearer " + token);
        //if (Storage.Auth() != null)
        //{
        //    request.SetRequestHeader("accessToken", $"{Storage.Auth().AccessToken}" );
        //}
        return request;
    }
    public UnityWebRequest Put(string MethodName, string Payload)
    {
        
        UnityWebRequest request = UnityWebRequest.Put($"{this._baseUrl}/{MethodName}", Payload); // Put can't send empty data
        if (!String.IsNullOrEmpty(Payload))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(Payload);
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        }
        
        request.SetRequestHeader("content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        
        //if (Storage.Auth() != null)
        //{
        //    request.SetRequestHeader("accessToken", $"{Storage.Auth().AccessToken}");
        //}
        return request;
    }
    public UnityWebRequest Get(string MethodName)
    {
        Debug.Log("Web Request Get : " + $"{this._baseUrl}/{MethodName}");
        UnityWebRequest request = UnityWebRequest.Get($"{this._baseUrl}/{MethodName}");
        request.SetRequestHeader("content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        //if (!String.IsNullOrEmpty(Storage.Auth().AccessToken))
        //{
        //    request.SetRequestHeader("accessToken", $"{Storage.Auth().AccessToken}");
        //}
        return request;
    }

    public UnityWebRequest GetWithToken(string MethodName,string token)
    {
        Debug.Log("Web Request Get : " + $"{this._baseUrl}/{MethodName}");
        UnityWebRequest request = UnityWebRequest.Get($"{this._baseUrl}/{MethodName}");
        request.SetRequestHeader("content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        request.SetRequestHeader("authentication", "Bearer " + token);
        //if (!String.IsNullOrEmpty(Storage.Auth().AccessToken))
        //{
        //    request.SetRequestHeader("accessToken", $"{Storage.Auth().AccessToken}");
        //}
        return request;
    }
}

