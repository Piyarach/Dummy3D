
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class MainMenuManager : MonoBehaviour
{
    private static MainMenuManager instance;
    public static MainMenuManager Instance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<MainMenuManager>();
        }
        return instance;
    }

    public TMP_InputField userIDInputField;
    public TMP_InputField usernameInputField;
    public TMP_Dropdown gameModeDropdown;
    public Button playButton;
    public Button canclePlayButton;
    public GameObject waitingPanel;

    bool isConnecting;
    bool isConnected;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        waitingPanel.SetActive(false);
        canclePlayButton.interactable = false;
        string userID = "";
        string userName = "";
        if (PlayerPrefs.HasKey("UserID"))
        {
            userID = PlayerPrefs.GetString("UserID");
        }
        if (PlayerPrefs.HasKey("UserName"))
        {
            userName = PlayerPrefs.GetString("UserName");
        }
        userIDInputField.text = userID;
        usernameInputField.text = userName;
        if (usernameInputField.text != "" && userIDInputField.text != "")
        {
            playButton.interactable = true;
        }
        else
        {
            playButton.interactable = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnGenUserIDClick()
    {
        System.Guid myuuid = System.Guid.NewGuid();
        userIDInputField.text = myuuid.ToString();
        if(usernameInputField.text != "")
        {
            playButton.interactable = true;
        }
        else
        {
            playButton.interactable = false;
        }
    }

    public void OnUserNameInputUpdate()
    {
        if (usernameInputField.text != "" && userIDInputField.text != "")
        {
            playButton.interactable = true;
        }
        else
        {
            playButton.interactable = false;
        }
    }

    public void OnPlayButtonClick()
    {
        waitingPanel.SetActive(true);
        isConnecting = true;
        isConnected = false;
        if (WebSocketManager.Instance())
        {
            WebSocketManager.Instance().StartConnectWebSocket();
        }
    }

    public void OnCancleJoinSessionClick()
    {
        if (isConnected)
        {
            WSPlayerData wsPlayerData = new WSPlayerData();
            wsPlayerData.playerID = userIDInputField.text;
            wsPlayerData.playerName = usernameInputField.text;
            WebSocketMessage webSocketMessage = new WebSocketMessage();
            webSocketMessage.msg = "leave_session";
            webSocketMessage.property = JObject.FromObject(wsPlayerData);

            string json = JsonConvert.SerializeObject(webSocketMessage);
            WebSocketManager.Instance().SendWebSocketMessage(json);
        }
    }

    public void OnWebSocketConnected()
    {
        if (isConnecting)
        {
            isConnecting = false;
            isConnected = true;
            WSJoinSession wsJoinSession = new WSJoinSession();
            wsJoinSession.playerID = userIDInputField.text;
            wsJoinSession.playerName = usernameInputField.text;
            wsJoinSession.gameSetting = new GameRule();
            wsJoinSession.gameSetting.totalPlayers = 4;
            wsJoinSession.gameSetting.gameType = "single_match";
            WebSocketMessage webSocketMessage = new WebSocketMessage();
            webSocketMessage.msg = "join_session";
            webSocketMessage.property = JObject.FromObject(wsJoinSession);

            string json = JsonConvert.SerializeObject(webSocketMessage);
            WebSocketManager.Instance().SendWebSocketMessage(json);
        }
    }

    public void OnSessionJoined()
    {

    }

    public void OnSessionReady(string sessionID)
    {
        Datamanager.sessionID = sessionID;
        Datamanager.userID = userIDInputField.text;
        Datamanager.userName = usernameInputField.text;
        canclePlayButton.interactable = false;
        SceneManager.LoadScene(1);
        PlayerPrefs.SetString("UserID", userIDInputField.text);
        PlayerPrefs.SetString("UserName", usernameInputField.text);
        PlayerPrefs.Save();
    }
}
