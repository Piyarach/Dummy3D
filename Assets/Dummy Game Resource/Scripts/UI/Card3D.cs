using cakeslice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card3D : MonoBehaviour
{
    enum SELECTABLE_TYPE
    {
        NONE = 0,
        SINGLE_CARD,
        MELD_SET
    }
    public Renderer cardRenderer;
    public int cardFrontMaterialIndex;
    public SpriteRenderer cardFilterImage;
    public SpriteRenderer crownIconImage;
    public Outline outline;
    public GameObject cardObjectAnimationRoot;
    public CardTweenAnimation cardTweenAnimation;
    public BoxCollider boxCollider;

    public DummyCard card;

    bool isInteractable = false;
    bool isSelected = false;
    bool isPermanentSelect = false;
    GameObject caller;
    string callbackMSG;
    private bool isTemporary = false;
    SELECTABLE_TYPE selectableType;
    // Start is called before the first frame update
    void Start()
    {
        if(card != null)
        {
            SetCard(card);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetCard(DummyCard _card)
    {
        card = _card;
        isSelected = false;
        Texture cardTexture = CardMaterialManager.Instance().GetCardTexture(card);
        if (cardTexture != null && cardRenderer != null && cardFrontMaterialIndex < cardRenderer.materials.Length)
        {
            cardRenderer.materials[cardFrontMaterialIndex].mainTexture = cardTexture;
        }
        if(outline) 
            outline.enabled = false;

        if (card.IsHead())
        {
            crownIconImage.gameObject.SetActive(true);
        }
        else
        {
            crownIconImage.gameObject.SetActive(false);
        }

        if (card.IsLayOff())
        {
            cardFilterImage.gameObject.SetActive(true);
        }
        else
        {
            cardFilterImage.gameObject.SetActive(false);
        }

        if (card.isTemporary)
        {
            Color c = cardRenderer.materials[cardFrontMaterialIndex].color;
            c.a = 0.5f;
            cardRenderer.materials[cardFrontMaterialIndex].color = UIManager.Instance().temporaryCardColorFilter;
        }
        else
        {
            cardRenderer.materials[cardFrontMaterialIndex].color = UIManager.Instance().normalCardColorFilter;
        }
    }

    public void UpdatePosition(Vector3 moveToPosition,Vector3 targetEuluAngle)
    {
        cardTweenAnimation.MoveTo(moveToPosition, 0.5f, "easeOutExpo", true, null, "");
        cardTweenAnimation.RotateTo(targetEuluAngle, 0.5f, "easeOutExpo", true, null, "");
    }

    public void OnCardClick()
    {
        if (isInteractable)
        {
            //Debug.Log("OnCardClick");
            if(selectableType == SELECTABLE_TYPE.SINGLE_CARD)
            {
                if (isSelected)
                {
                    SetUnSelected();
                }
                else if (!isSelected)
                {
                    SetSelected();
                }
                if (caller != null && callbackMSG != "")
                {
                    caller.SendMessage(callbackMSG, this);
                }
            }
            else if (selectableType == SELECTABLE_TYPE.MELD_SET)
            {
                if (caller != null && callbackMSG != "")
                {
                    caller.SendMessage(callbackMSG);
                }
            }
        }
    }

    public void OnCardHoverEnter()
    {
        if (isInteractable)
        {
            //Debug.Log("OnCardHoverEnter");
        }
    }

    public void OnCardHoverExit()
    {
        if (isInteractable)
        {
            //Debug.Log("OnCardHoverExit");
        }
    }

    public void AddOutline()
    {
        outline = cardRenderer.gameObject.AddComponent<Outline>();
        outline.enabled = false;
    }

    public void SetInteractable(bool interactable)
    {
        isInteractable = interactable;
    }

    public void SetSelectable(GameObject _caller,string _callbackMSG)
    {
        isInteractable = true;
        caller = _caller;
        callbackMSG = _callbackMSG;
        selectableType = SELECTABLE_TYPE.SINGLE_CARD;
    }

    public void SetSelectableMeldSet(GameObject _caller,string _callbackMSG)
    {
        isInteractable = true;
        caller = _caller;
        callbackMSG = _callbackMSG;
        selectableType = SELECTABLE_TYPE.MELD_SET;
    }

    public void SetDisableSelectable()
    {
        isInteractable = false;
    }

    public void SetSelected()
    {
        if (!isSelected)
        {
            isSelected = true;
            EnableOutline();
            MoveUp();
        }
    }

    public void SetUnSelected()
    {
        if (isSelected && !isPermanentSelect)
        {
            isSelected = false;
            DisaableOutline();
            MoveDown();
        }
    }

    void MoveUp()
    {
        Vector3 cardPos = transform.position + (transform.up * 0.25f);//cardRenderer.transform.localPosition;
        //cardPos.y += 0.5f;
        iTween.MoveTo(gameObject, iTween.Hash("name", "cardMoveUp", "position", cardPos, "easetype", "easeOutExpo", "time", 0.25f, "islocal", false));
    }

    void MoveDown()
    {
        Vector3 cardPos = transform.position - (transform.up * 0.25f);//Vector3 cardPos = cardRenderer.transform.localPosition;
        //cardPos.y = 0.0f;
        iTween.MoveTo(gameObject, iTween.Hash("name", "cardMoveDown", "position", cardPos, "easetype", "easeOutExpo", "time", 0.25f, "islocal", false));
    }

    public bool IsSelected()
    {
        return isSelected;
    }

    public void ResetCard()
    {
        isPermanentSelect = false;
        isSelected = false;
        isInteractable = false;
        if (outline)
        {
            outline.enabled = false;
        }
        SetTemporary(false);
    }

    public float GetCardWidth()
    {
        float width = boxCollider.size.x * cardRenderer.transform.localScale.x * transform.localScale.x;
        return width;
    }

    public float GetCardHeight()
    {
        float height = boxCollider.size.z * cardRenderer.transform.localScale.z * transform.localScale.y;
        return height;
    }

    public float GetCardGlobalHeight()
    {
        float height = boxCollider.size.z * cardRenderer.transform.lossyScale.z * transform.lossyScale.y;
        return height;
    }

    public void SetTemporary(bool _temporary)
    {
        if(isTemporary != _temporary)
        {
            isTemporary = _temporary;
            if (isTemporary)
            {
                cardRenderer.materials[cardFrontMaterialIndex].color = UIManager.Instance().temporaryCardColorFilter;
            }
            else
            {
                cardRenderer.materials[cardFrontMaterialIndex].color = UIManager.Instance().normalCardColorFilter;
            }
        }
    }

    public void SetPermanentSelect()
    {
        //Debug.Log("Card " + card.suit + ", " + card.rank + " SetPermanentSelect");
        isPermanentSelect = true;
        isInteractable = false;
        isSelected = true;
        if (outline)
        {
            outline.enabled = true;
        }
        MoveUp();
    }

    public void EnableOutline()
    {
        if (outline)
        {
            outline.enabled = true;
        }
        else
        {
            AddOutline();
            outline.enabled = true;
        }
    }

    public void DisaableOutline()
    {
        if (outline)
        {
            outline.enabled = false;
        }
    }
}
