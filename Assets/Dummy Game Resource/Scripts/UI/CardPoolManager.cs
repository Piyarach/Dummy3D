using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPoolManager : MonoBehaviour
{
    private static CardPoolManager instance;
    public static CardPoolManager Instance()
    {
        if(instance == null)
        {
            instance = FindObjectOfType<CardPoolManager>();
        }
        return instance;
    }
    public GameObject cardUIPrefab;

    private Stack<GameObject> objectPool = new Stack<GameObject>();

    private int currentObjectIndex = 0;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetCardObject()
    {
        GameObject obj = null;
        if(objectPool.Count == 0)
        {
            obj = Instantiate<GameObject>(cardUIPrefab);
        }
        else
        {
            obj = objectPool.Pop();
        }

        return obj;
    }

    public void DisableObject(GameObject obj)
    {
        if (obj.activeSelf)
        {
            obj.SetActive(false);
        }
        obj.transform.parent = transform;
        objectPool.Push(obj);
    }
}
