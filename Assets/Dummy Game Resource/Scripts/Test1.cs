using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Test1 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Test1 Start");
        Debug.Log(Datamanager.userID);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GotoTest2()
    {
        SceneManager.LoadScene("Test2");
    }
}
