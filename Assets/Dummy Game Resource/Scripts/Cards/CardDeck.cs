using System.Collections.Generic;
using UnityEngine;

public class CardDeck
{
    private SUITS[] deckSuits = { SUITS.CLUBS, SUITS.DIAMONS, SUITS.HEARTS, SUITS.SPADES };
    private RANK[] deckRank = { RANK.C_2, RANK.C_3, RANK.C_4, RANK.C_5, RANK.C_6, RANK.C_7, RANK.C_8, RANK.C_9, RANK.C_10, RANK.C_J, RANK.C_Q, RANK.C_K,RANK.C_A };

    private List<Card> deck;
    private bool isShuffle = false;
    public CardDeck()
    {
        deck = new List<Card>();
        for(int i = 0;i < deckSuits.Length; i++)
        {
            for (int j = 0; j < deckRank.Length; j++)
            {
                Card card = new Card();
                card.suit = deckSuits[i];
                card.rank = deckRank[j];
                deck.Add(card);
            }
        }
    }

    public void ShuffleDeck()
    {
        int shuffleCount = Random.Range(800, 1000);
        for(int i = 0;i < shuffleCount; i++)
        {
            bool isSameIndex = false;
            int fromIndex = -1;
            int toIndex = -1;
            do
            {
                isSameIndex = false;
                fromIndex = Random.Range(0, deck.Count);
                toIndex = Random.Range(0, deck.Count);
                if(fromIndex == toIndex)
                {
                    isSameIndex = true;
                }
            } while (isSameIndex);
            Card temp = deck[fromIndex];
            deck[fromIndex] = deck[toIndex];
            deck[toIndex] = temp;
        }
        isShuffle = true;
    }

    public bool IsShuffle()
    {
        return isShuffle;
    }

    public Stack<Card> GetDeckStack()
    {
        return new Stack<Card>(deck);
    }

    public Card[] GetDeckArray()
    {
        return deck.ToArray();
    }

    public void RemoveCard(Card card)
    {
        for(int i = 0;i < deck.Count; i++)
        {
            if(deck[i].rank == card.rank && deck[i].suit == card.suit)
            {
                deck.RemoveAt(i);
                break;
            }
        }
    }

    public void AddNewCard(Card card)
    {
        deck.Add(card);
    }
}
