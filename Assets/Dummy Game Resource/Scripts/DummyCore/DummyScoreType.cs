public enum DUMMY_SCORE_TYPE
{
    KNOCK = 0,
    KNOCK_SPETO,
    KNOCK_DARK,
    KNOCK_COLOR,
    MELD_HEAD,
    FOOL,
    FOOL_SPETO,
    DROP_HEAD,
    DROP_MELD_SPETO,
    DROP_LAYOFF,
    FALL_OUT
}

public static class DummyScoreType
{
    public static int ScoreByType(DUMMY_SCORE_TYPE scoreType)
    {
        int score = 0;
        if(scoreType == DUMMY_SCORE_TYPE.KNOCK)
        {
            score = 50;
        }
        else if (scoreType == DUMMY_SCORE_TYPE.KNOCK_SPETO)
        {
            score = 100;
        }
        else if (scoreType == DUMMY_SCORE_TYPE.MELD_HEAD)
        {
            score = 50;
        }
        else if (scoreType == DUMMY_SCORE_TYPE.FOOL)
        {
            score = -50;
        }
        else if (scoreType == DUMMY_SCORE_TYPE.FOOL_SPETO)
        {
            score = -50;
        }
        else if (scoreType == DUMMY_SCORE_TYPE.DROP_HEAD)
        {
            score = -50;
        }
        else if (scoreType == DUMMY_SCORE_TYPE.DROP_MELD_SPETO)
        {
            score = -50;
        }
        else if (scoreType == DUMMY_SCORE_TYPE.DROP_LAYOFF)
        {
            score = -50;
        }
        return score;
    }
}
