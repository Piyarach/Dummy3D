using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyCoreSimulator : IDummyCore
{
    const int dummyPlayerNumber = 4;
    DummyPlayer[] dummyPlayers = new DummyPlayer[dummyPlayerNumber];

    Stack<Card> dummyCardDeck;
    List<DummyCard> discardDeck = new List<DummyCard>();
    const int startCardFourPlayer = 7;

    int startPlayer = 0;
    int loopDirection = 1; // 1 = clockwise , -1 counterclcokwise
    int currentPlayer = 0;
    DummyCard currentDiscardCardSelectedCard;
    
    public DummyPlayer[] GetPlayers()
    {
        return dummyPlayers;
    }

    public void PlayerCallDrawFromDeck(string playerID)
    {
        if(dummyCardDeck.Count > 0)
        {
            Card card = dummyCardDeck.Pop();
            DummyCard dummyCard = DummyCard.CardToDummyCard(card.suit, card.rank);
            int cardInHandAmount = 0;
            for(int i =0;i < dummyPlayers.Length; i++)
            {
                if(dummyPlayers[i].playerID == playerID)
                {
                    DummyCard[] dummyCards = { dummyCard };
                    dummyPlayers[i].AddCardsToHand(dummyCards);
                    cardInHandAmount = dummyPlayers[i].GetHandCards().Count;
                    break;
                }
            }
            DummyGameManager.Instance().DummyCoreUserDrawEnd(dummyCard, playerID);
        }
        else
        {
            int cardInHandAmount = 0;
            for (int i = 0; i < dummyPlayers.Length; i++)
            {
                if (dummyPlayers[i].playerID == playerID)
                {
                    cardInHandAmount = dummyPlayers[i].GetHandCards().Count;
                    break;
                }
            }
            DummyGameManager.Instance().DummyCoreUserDrawEnd(null, playerID);
        }
    }

    public void PlayerCallDrawFromDiscardDeck(string playerID, int startIndex)
    {
        throw new System.NotImplementedException();
    }

    public void PlayerCallLayOff(string callerPlayerID,string ownerSetPlayerID, int meldSetIndex,DummyCard card)
    {
        Debug.Log("PlayerCallLayOff " + callerPlayerID);
        int callerIndex = 0;
        int ownerMeldSetIndex = 0;

        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == callerPlayerID)
            {
                // update caller hand card and score
                dummyPlayers[i].RemoveCardFromHand(card);
                callerIndex = i;
                dummyPlayers[i].AddLayOffCard(card);
            }
            if (dummyPlayers[i].playerID == ownerSetPlayerID)
            {
                card.AddLayOffTag();
                card.owner = callerPlayerID;
                card.isTemporary = false;
                dummyPlayers[i].GetMeldSet()[meldSetIndex].AddCardToSet(card);
                ownerMeldSetIndex = i;
            }
        }
        UpdatePlayerScore(callerPlayerID);
        DummyGameManager.Instance().DummyCoreUserCallLayOffEnd(ownerSetPlayerID, meldSetIndex, callerPlayerID, card, dummyPlayers[callerIndex].score);
        //DummyGameManager.Instance().UpdatePlayerData(dummyPlayers[callerIndex]);
        /*if(callerIndex != ownerMeldSetIndex)
        {
            DummyGameManager.Instance().UpdatePlayerData(dummyPlayers[ownerMeldSetIndex]);
        }*/
    }


    public void PlayerCallMeldFromHand(string playerID, DummyCard[] meldCardSet)
    {
        MeldSet meldSet = new MeldSet();
        meldSet.SetMeldCardSet(meldCardSet);
        dummyPlayers[currentPlayer].RemoveCardFromHand(meldCardSet);
        dummyPlayers[currentPlayer].AddMeldSet(meldSet);
        foreach (DummyCard card in meldCardSet)
        {
            if (card.IsHead())
            {
                dummyPlayers[currentPlayer].AddDummyScoreType(DUMMY_SCORE_TYPE.MELD_HEAD);
                break;
            }
        }
        UpdatePlayerScore(playerID);
        DummyGameManager.Instance().DummyCoreMeldFromHandEnd(playerID,meldCardSet, dummyPlayers[currentPlayer].score);
    }

    public void PlayerCallMeldFromDrawDiscardDeck(string playerID, DummyCard[] meldCardSet,DummyCard discardCard)
    {
        Debug.Log("DummyCoreSimulator : PlayerCallMeldFromDrawDiscardDeck");
        for(int i = 0;i < discardDeck.Count; i++)
        {
            if (discardDeck[i].Equal(discardCard))
            {
                List<DummyCard> drawCards = discardDeck.GetRange(i, discardDeck.Count - i);
                discardDeck.RemoveRange(i, discardDeck.Count - i);
                dummyPlayers[currentPlayer].AddCardsToHand(drawCards.ToArray());
                dummyPlayers[currentPlayer].RemoveCardFromHand(meldCardSet);
                MeldSet meldSet = new MeldSet();
                meldSet.SetMeldCardSet(meldCardSet);
                dummyPlayers[currentPlayer].AddMeldSet(meldSet);
                foreach(DummyCard card in meldCardSet)
                {
                    if (card.IsHead())
                    {
                        dummyPlayers[currentPlayer].AddDummyScoreType(DUMMY_SCORE_TYPE.MELD_HEAD);
                        break;
                    }
                }
                UpdatePlayerScore(playerID);
                CheckFoolHeadPlayer(meldSet.GetMeldCardSet());
                CheckFoolSpetoPlayer(meldSet.GetMeldCardSet());
                DummyGameManager.Instance().DummyCoreMeldFromDrawDiscardDeckEnd(playerID, drawCards.ToArray(), meldCardSet, dummyPlayers[currentPlayer].score);
                break;
            }
        }
    }

    public void PlayerCallKnock(string playerID)
    {
        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == playerID && dummyPlayers[i].GetHandCards().Count == 1 && !dummyPlayers[i].isKnocker)
            {
                dummyPlayers[i].isKnocker = true;
                if (dummyPlayers[i].isFirstMeld)
                {
                    dummyPlayers[i].AddDummyScoreType(DUMMY_SCORE_TYPE.KNOCK_DARK);
                }
            }
        }
    }


    void PlayerCallMeldFromHandEnd()
    {
        Debug.Log("PlayerCallMeldFromHandEnd Score " + dummyPlayers[currentPlayer].score);
        //DummyGameManager.Instance().DummyCoreSelectAvailableMeldSetEnd(null,dummyPlayers[currentPlayer]);
    }

    

    public void PlayerDiscardCard(string playerID, DummyCard card)
    {
        card.owner = playerID;
        discardDeck.Add(card);
        int playerIndex = currentPlayer;
        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == playerID)
            {
                dummyPlayers[i].RemoveCardFromHand(card);
                dummyPlayers[i].isFirstMeld = false;
                CheckFoolDiscardLayOff(card);
                UpdatePlayerScore(playerID);
                DummyGameManager.Instance().DummyCoreDiscardEnd(card, playerID, dummyPlayers[i].score);
                break;
            }
        }
        
    }

    public void PlayerEndTurn(string playerID)
    {
        Debug.Log("Player " + playerID + " EndTurn");
        currentPlayer += loopDirection;
        if (currentPlayer >= dummyPlayers.Length)
        {
            currentPlayer = 0;
        }
        else if (currentPlayer < 0)
        {
            currentPlayer = dummyPlayers.Length - 1;
        }
        DummyPlayer activePlayer = dummyPlayers[currentPlayer];
        PlayerNext();
    }

    public void PlayerNext()
    {
        // remove player owner id form card when round return back to player
        foreach(DummyCard dummyCard in discardDeck)
        {
            if(dummyCard.owner == dummyPlayers[currentPlayer].playerID)
            {
                dummyCard.owner = "";
            }
        }
        DummyGameManager.Instance().PlayerStartSimulator(dummyPlayers[currentPlayer].playerID, dummyPlayers[currentPlayer].GetHandCards().ToArray());
    }

    public void ResetDeck()
    {
        dummyCardDeck.Clear();
        CardDeck cardDeck = new CardDeck();
        cardDeck.ShuffleDeck();
        dummyCardDeck = cardDeck.GetDeckStack();
    }

    public void ShuffleDeck()
    {
        throw new System.NotImplementedException();
    }

    public void StartDealCardToAllPlayer()
    {
        if(dummyCardDeck.Count == 0)
        {
            CardDeck cardDeck = new CardDeck();
            cardDeck.ShuffleDeck();
            dummyCardDeck = cardDeck.GetDeckStack();
        }
        int cardCount = startCardFourPlayer * dummyPlayers.Length;
        int playerIndex = startPlayer;
        while(cardCount > 0)
        {
            Card card = dummyCardDeck.Pop();
            dummyPlayers[playerIndex].AddCardToHand(DummyCard.CardToDummyCard(card.suit,card.rank));
            playerIndex += loopDirection;
            if(playerIndex >= dummyPlayers.Length)
            {
                playerIndex = 0;
            }
            else if(playerIndex < 0)
            {
                playerIndex = dummyPlayers.Length - 1;
            }
            cardCount--;
        }
        DummyGameManager.Instance().DummyCoreDealCardToPlayersEnd(dummyPlayers[currentPlayer].playerID, dummyPlayers[currentPlayer].GetHandCards().ToArray());
    }

    public void UpdatePlayerHandCard(string playerID, DummyCard[] cards)
    {
        throw new System.NotImplementedException();
    }

    public void UpdatePlayerMeldSet(string playerID, MeldSet[] meldSets)
    {
        throw new System.NotImplementedException();
    }
    public void Initial()
    {
        DummyPlayer player1 = new DummyPlayer();
        player1.playerID = Datamanager.userID;
        player1.playerName = "Gao Jing"; 
        dummyPlayers[0] = player1;

        DummyPlayer player2 = new DummyPlayer();
        player2.playerID = "1111";
        player2.playerName = "Jack Ryan";
        dummyPlayers[1] = player2;

        DummyPlayer player3 = new DummyPlayer();
        player3.playerID = "3434";
        player3.playerName = "Sarah";
        dummyPlayers[2] = player3;

        DummyPlayer player4 = new DummyPlayer();
        player4.playerID = "5555";
        player4.playerName = "John Doe";
        dummyPlayers[3] = player4;

        /*CardDeck cardDeck = new CardDeck();
        cardDeck.ShuffleDeck();
        dummyCardDeck = cardDeck.GetDeckStack();
        */
        SetTestDeck();
        startPlayer = 0;// Random.Range(0, dummyPlayerNumber);
        currentPlayer = startPlayer;
        DummyGameManager.Instance().DummyCoreIntialEnd(dummyPlayers, startPlayer, dummyCardDeck.Count);
    }

    public void SetTestDeck()
    {
        CardDeck cardDeck = new CardDeck();

        List<DummyCard> playerCards = new List<DummyCard>();
        //Player 1 Hand Cards
        playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_3));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_7));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_K));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_J));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_J));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_10));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_Q));

        //Player 2 Hand Cards
        playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_4));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_6));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_A));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_4));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_3));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_8));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_9));

        //Player 3 Hand Cards
        playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_K));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_5));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_5));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_6));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_J));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_2));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_Q));

        //Player 4 Hand Cards
        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_5));//playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_10)); ;//

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_Q));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_8));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_8));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_Q));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_4)); //playerCards.Add(DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_10));

        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_10));

        // Deck Card 
        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_7)); // Head
        playerCards.Add(DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_4));
        playerCards.Add(DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_5));

        for(int i = 0;i < playerCards.Count; i++)
        {
            cardDeck.RemoveCard(playerCards[i]);
        }
        cardDeck.ShuffleDeck();

        //int c = 0;
        int maxC = 4 * 7;
        for (int i = playerCards.Count-1;i >= maxC; i--)
        {
            cardDeck.AddNewCard(playerCards[i]);
        }
        
        for(int c = 0; c < 7; c++)
        {
            for(int p = 3;p >= 0; p--)
            {
                int index = c + (p*7);
                //Debug.Log("c " + c + ", p " + p + " , index " + index);
                cardDeck.AddNewCard(playerCards[index]);
            }
        }

        dummyCardDeck = cardDeck.GetDeckStack();
    }
    public void InitialTest1()
    {
        CardDeck cardDeck = new CardDeck();

        DummyPlayer player1 = new DummyPlayer();
        player1.playerID = Datamanager.userID;
        player1.playerName = "Gao Jing";
        dummyPlayers[0] = player1;

        DummyPlayer player2 = new DummyPlayer();
        player2.playerID = "1111";
        player2.playerName = "Jack Ryan";
        dummyPlayers[1] = player2;

        DummyPlayer player3 = new DummyPlayer();
        player3.playerID = "3434";
        player3.playerName = "Sarah";
        dummyPlayers[2] = player3;

        DummyPlayer player4 = new DummyPlayer();
        player4.playerID = "5555";
        player4.playerName = "John Doe";
        dummyPlayers[3] = player4;

        currentPlayer = 0;

        //Player 1 Hand Cards
        DummyCard h1 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_3);

        DummyCard h2 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_7);

        DummyCard h3 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_9);

        DummyCard h4 = DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_J);

        DummyCard h5 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_J);

        DummyCard h6 = DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_7);

        DummyCard h7 = DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_Q);

        DummyCard[] p1HandCards = { h1, h2 ,h3,h4,h5,h6,h7 };
        dummyPlayers[0].AddCardsToHand(p1HandCards);

        //Player 2 Hand Cards
        h1 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_4);

        h2 = DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_Q);

        h3 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_6);

        h4 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_7);

        DummyCard[] p2HandCards = { h1, h2, h3,h4};
        dummyPlayers[1].AddCardsToHand(p2HandCards);

        //Player 3 Hand Cards
        h1 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_Q);

        h2 = DummyCard.CardToDummyCard(SUITS.DIAMONS, RANK.C_K);

        h3 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_10);

        DummyCard[] p3HandCards = { h1, h2, h3 };
        dummyPlayers[2].AddCardsToHand(p3HandCards);

        //Player 4 Hand Cards
        h1 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_K);

        h2 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_A);

        h3 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_A);

        DummyCard[] p4HandCards = { h1, h2, h3 };
        dummyPlayers[3].AddCardsToHand(p4HandCards);

        //Discard Deck
        DummyCard d1 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_5);
        d1.AddHeadTag();
        DummyCard d2 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_8);
        DummyCard d3 = DummyCard.CardToDummyCard(SUITS.SPADES, RANK.C_10);
        DummyCard d4 = DummyCard.CardToDummyCard(SUITS.CLUBS, RANK.C_7);
        DummyCard d5 = DummyCard.CardToDummyCard(SUITS.HEARTS, RANK.C_J);
        discardDeck.Add(d1);
        discardDeck.Add(d2);
        discardDeck.Add(d3);
        discardDeck.Add(d4);
        discardDeck.Add(d5);

        foreach(DummyCard card in dummyPlayers[0].GetHandCards())
        {
            cardDeck.RemoveCard(card);
        }
        foreach (DummyCard card in dummyPlayers[1].GetHandCards())
        {
            cardDeck.RemoveCard(card);
        }
        foreach (DummyCard card in dummyPlayers[2].GetHandCards())
        {
            cardDeck.RemoveCard(card);
        }
        foreach (DummyCard card in dummyPlayers[3].GetHandCards())
        {
            cardDeck.RemoveCard(card);
        }

        foreach (DummyCard card in discardDeck)
        {
            cardDeck.RemoveCard(card);
        }

        cardDeck.ShuffleDeck();
        dummyCardDeck = cardDeck.GetDeckStack();

        DummyGameManager.Instance().DummyCoreInitialTestEnd(dummyPlayers, discardDeck, currentPlayer, dummyCardDeck.Count);
    }

    public int GetDeckRemainCardNumber()
    {
        return dummyCardDeck.Count;
    }

    public void DiscardHeadCard(string playerID)
    {
        Card card = dummyCardDeck.Pop();
        DummyCard dummyCard = DummyCard.CardToDummyCard(card.suit,card.rank);
        dummyCard.AddHeadTag();
        discardDeck.Add(dummyCard);
        DummyGameManager.Instance().DummyCoreDiscardHeadCardEnd(dummyCard);
    }

    public void StartGame(string playerID)
    {
        Debug.Log("DummyCoreSimulator StartRound");
        // must check for every players have ready state in server mode
        DummyPlayer activePlayer = dummyPlayers[currentPlayer];
        DummyGameManager.Instance().PlayerStartSimulator(activePlayer.playerID,activePlayer.GetHandCards().ToArray());
    }

    /*
    public void StartCollectCardFromDiscardDeck(string playerID, DummyCard startCollectedCard)
    {
        currentDiscardCardSelectedCard = startCollectedCard;
        List<DummyCard> tempHandCards = new List<DummyCard>();
        int startCollectIndex = 0;
        for(int i = 0;i < discardDeck.Count; i++)
        {
            if (discardDeck[i].Equal(startCollectedCard))
            {
                startCollectIndex = i;
            }
        }
        for (int i = startCollectIndex; i < discardDeck.Count; i++)
        {
            tempHandCards.Add(discardDeck[i]);
        }
        for (int i = 0; i < dummyPlayers[currentPlayer].GetHandCards().Count; i++)
        {
            tempHandCards.Add(dummyPlayers[currentPlayer].GetHandCards()[i]);
        }
        FindMeldSetFromHand(tempHandCards);
    }
    */
    

    void UpdatePlayerScore(string playerID)
    {
        int score = 0;
        int playerIndex = -1;
        for (int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == playerID)
            {
                // Score From Meld Set
                foreach(MeldSet meldSet in dummyPlayers[i].GetMeldSet())
                {
                    DummyCard[] cards = meldSet.GetMeldCardSet();
                    for (int cI = 0;cI < cards.Length; cI++)
                    {
                        if (!cards[cI].IsLayOff())
                        {
                            score += cards[cI].GetCardScore();
                        }
                        
                    }
                }
                // Score From Special Score
                foreach(DUMMY_SCORE_TYPE scoreType in dummyPlayers[i].GetDummyScoreType())
                {
                    score += DummyScoreType.ScoreByType(scoreType);
                }
                playerIndex = i;
            }

            foreach(MeldSet meldSet in dummyPlayers[i].GetMeldSet())
            {
                DummyCard[] cards = meldSet.GetMeldCardSet();
                for (int cI = 0;cI < cards.Length; cI++)
                {
                    if(cards[cI].IsLayOff() && cards[cI].owner == playerID)
                    {
                        score += cards[cI].GetCardScore();
                    }
                }
            }
        }
        if(playerIndex != -1)
        {
            dummyPlayers[playerIndex].score = score;
        }
    }

    bool IsLayOffAvailable(DummyCard layOffCard,DummyCard[] meldSet)
    {
        bool isLayOffAvailable = false;
        List<DummyCard> cards = new List<DummyCard>(meldSet);
        cards.Add(layOffCard);
        isLayOffAvailable = IsMatchSet(cards.ToArray()) || IsRunSet(cards.ToArray());
        return isLayOffAvailable;
    }

    void CheckFoolHeadPlayer(DummyCard[] meldCards)
    {
        List<string> owners = new List<string>();
        bool isHead = false;
        for(int i = 0;i < meldCards.Length; i++)
        {
            //Debug.Log("CheckFoolHeadPlayer " + meldCards[i].suit + "," + meldCards[i].rank + " , isHead:" + meldCards[i].IsHead() + " ,owner:" + meldCards[i].owner);
            if(meldCards[i].owner != "")
            {
                owners.Add(meldCards[i].owner);
            }
            if (meldCards[i].IsHead() && !meldCards[i].IsLayOff())
            {
                isHead = true;
            }
        }
        if(isHead && owners.Count > 0)
        {
            for(int i = 0;i < owners.Count; i++)
            {
                for (int pI = 0; pI < dummyPlayers.Length; pI++)
                {
                    if (dummyPlayers[pI].playerID == owners[i])
                    {
                        AddPlayerSpecialScoreType(dummyPlayers[pI], DUMMY_SCORE_TYPE.DROP_HEAD);
                        break;
                    }
                }
            }
        }
    }

    void CheckFoolSpetoPlayer(DummyCard[] meldCards)
    {
        List<string> owners = new List<string>();
        bool isSpeto = false;
        for (int i = 0; i < meldCards.Length; i++)
        {
            //Debug.Log("CheckFoolHeadPlayer " + meldCards[i].suit + "," + meldCards[i].rank + " , isHead:" + meldCards[i].IsHead() + " ,owner:" + meldCards[i].owner);
            if (meldCards[i].owner != "")
            {
                owners.Add(meldCards[i].owner);
            }
            if (meldCards[i].IsSpeto() && !meldCards[i].IsLayOff())
            {
                isSpeto = true;
            }
        }
        if (isSpeto && owners.Count > 0)
        {
            for (int i = 0; i < owners.Count; i++)
            {
                for (int pI = 0; pI < dummyPlayers.Length; pI++)
                {
                    if (dummyPlayers[pI].playerID == owners[i])
                    {
                        AddPlayerSpecialScoreType(dummyPlayers[pI], DUMMY_SCORE_TYPE.DROP_MELD_SPETO);
                        break;
                    }
                }
            }
        }
    }

    void CheckFoolDiscardLayOff(DummyCard discardCard)
    {
        bool isDiscardLayOff = false;
        for(int pI = 0;pI < dummyPlayers.Length; pI++)
        {
            List<MeldSet> meldSets = dummyPlayers[pI].GetMeldSet();
            foreach(MeldSet meldSet in meldSets)
            {
                if (IsLayOffAvailable(discardCard, meldSet.GetMeldCardSet()))
                {
                    isDiscardLayOff = true;
                    break;
                }
            }
            if (isDiscardLayOff)
            {
                break;
            }
        }
        if (isDiscardLayOff)
        {
            for (int pI = 0; pI < dummyPlayers.Length; pI++) { 
                if(dummyPlayers[pI].playerID == discardCard.owner)
                {
                    AddPlayerSpecialScoreType(dummyPlayers[pI], DUMMY_SCORE_TYPE.DROP_LAYOFF);
                    break;
                }
            }
        }
    }

    public static bool IsMeldSet(DummyCard[] cards)
    {
        bool isMeldSet = false;
        if(IsMatchSet(cards) || IsRunSet(cards))
        {
            isMeldSet = true;
        }
        return isMeldSet;
    }

    public static bool IsMatchSet(DummyCard[] cards)
    {
        bool isMatchSet = true;
        if (cards.Length >= 3)
        {
            // check suit of first card and other cards 
            for (int i = 1; i < cards.Length; i++)
            {
                if (cards[0].rank != cards[i].rank)
                {
                    isMatchSet = false;
                    break;
                }
            }
        }
        return isMatchSet;
    }

    public static bool IsRunSet(DummyCard[] cards)
    {
        bool isRunSet = true;
        if (cards.Length >= 3)
        {
            // sort form low to hight first
            for (int i = 0; i < cards.Length - 1; i++)
            {
                for (int j = i + 1; j < cards.Length; j++)
                {
                    if (cards[i].suit != cards[j].suit)
                    {
                        isRunSet = false;
                        break;
                    }
                    if (cards[i].GetRankNumber() > cards[j].GetRankNumber())
                    {
                        DummyCard temp = cards[i];
                        cards[i] = cards[j];
                        cards[j] = temp;
                    }
                }
            }
            if (isRunSet)
            {
                for (int i = 0; i < cards.Length - 1; i++)
                {
                    if (cards[i + 1].GetRankNumber() - cards[i].GetRankNumber() != 1)
                    {
                        isRunSet = false;
                        break;
                    }
                }
            }
            
        }
        return isRunSet;
    }

    void AddPlayerSpecialScoreType(DummyPlayer player,DUMMY_SCORE_TYPE score_type)
    {
        player.AddDummyScoreType(score_type);
        UpdatePlayerScore(player.playerID);
        DummyGameManager.Instance().OnUpdatePlayerScore(player.playerID, player.score, score_type);
    }
}
