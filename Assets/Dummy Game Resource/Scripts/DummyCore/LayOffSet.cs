using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayOffSet
{
    MeldSet meldSet;
    DummyCard layOffCard;
    int meldSetIndex;

    public LayOffSet(MeldSet _meldSet,DummyCard _dummyCard,int _meldSetIndex)
    {
        meldSet = _meldSet;
        layOffCard = _dummyCard;
        meldSetIndex = _meldSetIndex;
    }

    public MeldSet MeldSet()
    {
        return meldSet;
    }

    public DummyCard LayOffCard()
    {
        return layOffCard;
    }

    public int MeldSetIndex()
    {
        return meldSetIndex;
    }
}
